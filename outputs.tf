output "external_instance_ip" {
  value = google_compute_instance.this.network_interface.0.access_config.0.nat_ip
}

output "internal_instance_ip" {
  value = google_compute_instance.this.network_interface.0.network_ip
}
