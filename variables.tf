variable "instance_name" {
  description = "Instance name"
  type        = string
}

variable "instance_vpc_subnet_id" {
  description = "Subnet vpc id for the instance"
  type        = string
}

variable "instance_zone" {
  description = "The zone that the machine should be created in"
  type        = string
}

variable "instance_machine_type" {
  description = "Instance machine type"
  type        = string
  default     = "e2-medium"
}

variable "is_instance_public" {
  description = "Is the instance supposed to have an external ip"
  type        = bool
  default     = true
}

variable "instance_tags" {
  description = "List of instance tags"
  default     = []
  type        = list(string)
}

variable "instance_labels" {
  description = "A set of key/value label pairs to assign to the instance."
  default     = {}
  type        = map(string)
}

variable "instance_image" {
  description = "Distro image for the instance"
  default     = "debian-10"
  type        = string
}

variable "is_instance_preemptible" {
  description = "Specifies if the instance is preemptible"
  default     = false
  type        = bool
}

variable "instance_disk_size" {
  description = "Instance disk size, GiB"
  type        = number
  default     = 11
}

variable "instance_ssh_user" {
  description = "Instance username for ssh access"
  type        = string
  default     = "user"
}

variable "instance_ssh_public_key_path" {
  description = "Instance ssh public key path for ssh access"
  type        = string
  default     = "~/.ssh/id_ed25519.pub"
}
