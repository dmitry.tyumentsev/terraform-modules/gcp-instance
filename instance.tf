resource "google_compute_instance" "this" {
  name         = var.instance_name
  machine_type = var.instance_machine_type
  zone         = var.instance_zone
  tags         = var.instance_tags
  labels       = var.instance_labels

  boot_disk {
    initialize_params {
      image = var.instance_image
      size  = var.instance_disk_size
    }
  }

  metadata = {
    ssh-keys = "${var.instance_ssh_user}:${file(var.instance_ssh_public_key_path)}"
  }

  network_interface {
    subnetwork = var.instance_vpc_subnet_id

    dynamic "access_config" {
      for_each = var.is_instance_public ? [1] : []

      content {
      }
    }
  }

  scheduling {
    preemptible = var.is_instance_preemptible
    automatic_restart = !var.is_instance_preemptible
  }
}
